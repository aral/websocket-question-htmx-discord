export default () => kitten.html`
<page htmx htmx-websocket>

<div id="ws-wrapper" hx-ext="ws" ws-connect="/simple.socket">
  <form id="form" ws-send style="display: inline;">
    <button type="submit" name="select_area_feature" value="cheese" hx-ws="send">
      <img src="/images/cheese-32.png" />
    </button>
  </form>
</div>
`
