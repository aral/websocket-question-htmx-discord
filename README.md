# Reproduction attempt for “My websocket submit button is sending the payload twice”

(See https://discord.com/channels/725789699527933952/1221975245947277443/1221975245947277443)

## Instructions

1. Install [Kitten](https://codeberg.org/kitten/app)
2. Clone this repository
3. From the local working copy folder, run `kitten`

Hit _https://localhost_ and press the button to see that only one request is being sent.

(Kitten includes htmx-2 and the latest WebSocket extension as well as a couple of additional features that aren’t in mainline htmx-2 yet but those should not affect this. I’d venture to think the issue is not present in htmx-2 and the latest htmx WebSocket extension.)

